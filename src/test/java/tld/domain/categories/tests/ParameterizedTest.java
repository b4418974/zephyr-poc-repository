package tld.domain.categories.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public final class ParameterizedTest {

    @org.junit.jupiter.params.ParameterizedTest
    @MethodSource("getBooleanStream")
    public void feature_scenario_expectedResult_010(boolean input) {
        Assertions.assertTrue(input);
    }

    public static Stream<Boolean> getBooleanStream() {
        Boolean[] array = new Boolean[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.random() >= 0.2;
        }

        return Stream.of(array);
    }
}
